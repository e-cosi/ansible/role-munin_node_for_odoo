# ansible-role-munin_node_for_odoo

Ansible role - Install munin node plugins for specific odoo metrics

## Usage

### Download role with Ansible Galaxy

#### - Directly in command line

 ``` bash
 # Command line
 ansible-galaxy install git+https://github.com/e-COSI/ansible-role-munin_node_for_odoo.git
 ```

#### - With a "requirements.yml" file

``` yaml
# requirements.yml
---
- src: ssh://git@github.com/e-COSI/ansible-role-munin_node_for_odoo.git
  version: master
  name: munin_node_for_odoo
  scm: git
```

``` bash
# Command line
ansible-galaxy install -r requirement.yml
```

## Role Variable

|Variable Name| Required | Default Value | Description |
|-------------|----------|---------------|-------------|
|munin_node_extra_plugins_dir|Yes|"/usr/share/munin/plugins/extras"|Path of munin node plugins directory|
|munin_node_odoo_plugins_active|Yes|<ul><li>odoo_response_time</li><li>odoo_tr_min</li><li>odoo_db_event_registration_sum</li><li>odoo_db_event_registration_activity</li><li>odoo_db_sale_order_activity</li><li>odoo_db_sale_order_sum</li><li>odoo_db_invoice</li><li>odoo_db_size</li></ul>|Plugins to activate|
|munin_node_for_odoo_db_host|Yes|"127.0.0.1"|IP address of Odoo database|
|munin_node_for_odoo_db_port|Yes|"5432"|Port of Odoo database|
|munin_node_for_odoo_db_name_prod|Yes|"ODOO_DB"|Name of Odoo database|
|munin_node_for_odoo_db_username|Yes|"odoo"|Username to access odoo database|
|munin_node_for_odoo_db_password|Yes|"odoo"|Password to access odoo database|
|munin_node_for_odoo_filestore_dir|Yes|/etc/odoo/data/filestore|Path of odoo filestore directory|
|munin_node_odoo_plugins_config|Yes|(see default/main.yml file)|Define configuration for each plugins|